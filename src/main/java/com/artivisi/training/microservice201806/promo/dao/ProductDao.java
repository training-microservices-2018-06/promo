package com.artivisi.training.microservice201806.promo.dao;

import com.artivisi.training.microservice201806.promo.entity.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {
}
