package com.artivisi.training.microservice201806.promo.controller;

import com.artivisi.training.microservice201806.promo.dao.ProductDao;
import com.artivisi.training.microservice201806.promo.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProductController {

    @Autowired private ProductDao productDao;

    @GetMapping("/product/")
    public Iterable<Product> semuaProduk() {
        return productDao.findAll();
    }

    @GetMapping("/hostinfo")
    public Map<String, Object> backendInfo(HttpServletRequest request) {
        Map<String, Object> info = new HashMap<>();
        info.put("hostname", request.getLocalName());
        info.put("ip", request.getLocalAddr());
        info.put("port", request.getLocalPort());
        return info;
    }
}
